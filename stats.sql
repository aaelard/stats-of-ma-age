CREATE OR REPLACE VIEW stats_users AS
SELECT u.id,
       strip_tags(u.spravce) AS spravce,
       u.provincie,
       u.barva,
       u.specializace,
       u.plocha,
       u.odehrano_tahu,
       u.brana_no,
       u.slava
FROM users u
WHERE odehrano_tahu > 2000;
SELECT *
FROM stats_users;

CREATE OR REPLACE VIEW stats_prva_utocnik AS
SELECT u.id,
       u.spravce,
       u.provincie,
       u.barva,
       u.specializace,
       u.plocha,
       u.odehrano_tahu,
       u.brana_no,
       u.slava,
       utoky.kdy,
       utoky.ztraty_kdo,
       utoky.ztraty_koho,
       utoky.zabrano_har,
       ROUND((utoky.sila_utocnika - utoky.sila_utocnika_end) / utoky.sila_utocnika * 100, 1)   AS ztraty_kdo_acc,
       ROUND((utoky.sila_obrance - utoky.sila_obrance_end) / utoky.sila_obrance * 100, 1)      AS ztraty_koho_acc,
       ROUND(((utoky.sila_obrance - utoky.sila_obrance_end) / utoky.sila_obrance -
              (utoky.sila_utocnika - utoky.sila_utocnika_end) / utoky.sila_utocnika) * 100, 1) AS ztraty_diff,
       IF((utoky.sila_obrance - utoky.sila_obrance_end) / utoky.sila_obrance > 0.9, 1, 0)      AS 90p
FROM utoky
     JOIN stats_users u ON u.id = utoky.kdo
WHERE je_to_prvoutok = 1
ORDER BY kdy;
SELECT *
FROM stats_prva_utocnik;

CREATE OR REPLACE VIEW stats_pocet_prv AS
SELECT kdo, COUNT(42) cnt, COUNT(42) * 86400 / (MAX(kdy) - MIN(kdy) + 1) as apd
FROM utoky
WHERE je_to_prvoutok = 1
GROUP BY kdo
HAVING COUNT(42) > 10;
SELECT *
FROM stats_pocet_prv;

CREATE OR REPLACE VIEW stats_prva_hraci_pocty AS
SELECT *,
       round(plocha, -4)           as plocha_10k,
       round(plocha / 5000) * 5000 as plocha_5k,
       round(slava / 200) * 200    as slava_200,
       round(slava, -2)            as slava_100,
       round(apd * 5) / 5          as apd_5
FROM stats_users u
     JOIN stats_pocet_prv p ON p.kdo = u.id;
SELECT *
FROM stats_prva_hraci_pocty;

-- podle brany
SELECT brana_no,
       count(distinct kdo) AS hracu,
       sum(cnt)            AS prv_celkem,
       avg(apd)            AS prv_denne_avg,
       std(apd)            AS prv_denne_std,
       min(apd)            AS prv_denne_min,
       max(apd)            AS prv_denne_max
FROM stats_prva_hraci_pocty
GROUP BY brana_no
ORDER BY brana_no DESC;

-- podle hliny
SELECT plocha_5k,
       count(distinct kdo) AS hracu,
       sum(cnt)                       AS prv_celkem,
       avg(apd)                       AS prv_denne_avg,
       std(apd)                       AS prv_denne_std,
       min(apd)                       AS prv_denne_min,
       max(apd)                       AS prv_denne_max
FROM stats_prva_hraci_pocty
GROUP BY plocha_5k
ORDER BY plocha_5k DESC;

-- podle slavy
SELECT slava_200,
       count(distinct kdo) AS hracu,
       sum(cnt)                       AS prv_celkem,
       avg(apd)                       AS prv_denne_avg,
       std(apd)                       AS prv_denne_std,
       min(apd)                       AS prv_denne_min,
       max(apd)                       AS prv_denne_max
FROM stats_prva_hraci_pocty
GROUP BY slava_200
ORDER BY slava_200 DESC;

-- podle APD
SELECT count(distinct kdo) AS hracu,
       apd_5               AS prv_denne,
       sum(cnt)            AS prv_celkem
FROM stats_prva_hraci_pocty
GROUP BY apd_5
ORDER BY apd_5 DESC;


-- uspesnost utoku podle barev
SELECT barva,
       specializace,
       COUNT(DISTINCT id)                       as pocet_hracu,
       COUNT(42)                                as pocet_prv,
       ROUND(COUNT(42) / COUNT(DISTINCT id), 1) AS prv_na_hrace,
       ROUND(AVG(ztraty_kdo_acc), 1)            AS avg_ztraty_utocnik,
       ROUND(AVG(ztraty_koho_acc), 1)           AS avg_ztraty_obrance,
       ROUND(AVG(ztraty_diff), 1)               AS avg_rozdil,
       SUM(90p)                                 AS za_min_90,
       ROUND(SUM(90p) / COUNT(42) * 100, 1)     AS za_min_90_pct
FROM stats_prva_utocnik
GROUP BY barva, specializace
ORDER BY barva, specializace;


-- uspesnost utoku podle barev (bez 90p)
SELECT barva,
       specializace,
       COUNT(DISTINCT id)                       as pocet_hracu,
       COUNT(42)                                as pocet_prv,
       ROUND(COUNT(42) / COUNT(DISTINCT id), 1) AS prv_na_hrace,
       ROUND(AVG(ztraty_kdo_acc), 1)            AS avg_ztraty_utocnik,
       ROUND(AVG(ztraty_koho_acc), 1)           AS avg_ztraty_obrance,
       ROUND(AVG(ztraty_diff), 1)               AS avg_rozdil
FROM stats_prva_utocnik
WHERE 90p = 0
GROUP BY barva, specializace
ORDER BY barva, specializace;

-- nasvaceno (bez vraceni, <90p)
SELECT barva,
       specializace,
       COUNT(DISTINCT id)                              as pocet_hracu,
       ROUND(SUM(zabrano_har) / COUNT(DISTINCT id), 0) as avg_nasvaceno,
       SUM(zabrano_har)                                as celkem_nasvaceno
FROM stats_prva_utocnik
WHERE 90p = 0
GROUP BY barva, specializace
ORDER BY barva, specializace;

-- prumerne final staty
SELECT barva,
       specializace,
       COUNT(42)               as pocet_hracu,
       ROUND(AVG(slava), 0)    as avg_slava,
       ROUND(AVG(brana_no), 1) as avg_brana,
       ROUND(AVG(plocha), -3)  as avg_hlina
FROM stats_users
GROUP BY barva, specializace
ORDER BY barva, specializace;